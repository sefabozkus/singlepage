import React, {Component} from 'react';
import {BrowserRouter as Router} from "react-router-dom";
import {Navbar} from "./Components/NavbarComponent";
import './App.css';


class App extends Component {

    constructor() {
        super();
        const path = window.location.pathname.split("/");
        if (path[1] === "") {
            this.state = {activeMenuItem: 'home'};
        }
        else {
            this.state = {activeMenuItem: path[1]};
            }
    }

    render() {

        return (
            <Router>
                <div className="App">
                    <Navbar activeMenuItem={this.state.activeMenuItem}/>
                </div>
            </Router>

        );
    }
}


export default App;
