import React from 'react';
import {Link, Route,} from "react-router-dom";
import {Grid, Icon, Menu} from 'semantic-ui-react'

let path = 'home';
let content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris at tempor nibh. Donec ut feugiat tellus,\n" +
    "            vestibulum imperdiet mi. Aenean arcu arcu, faucibus ac aliquam ac, feugiat sed metus. Vestibulum consectetur\n" +
    "            erat egestas lacus dapibus convallis. Morbi enim nibh, dictum vitae dignissim a, finibus vitae mauris. Cras\n" +
    "            vulputate sapien eu sapien viverra, nec eleifend mi rutrum. Quisque tempor lectus eu enim ornare, sit amet\n" +
    "            pretium velit semper. Nulla auctor augue ante, non imperdiet libero euismod vitae. Nam lacinia ex sed tellus\n" +
    "            accumsan tempor. Donec rhoncus eros nec neque maximus, at condimentum est gravida.\n" +
    "\n" +
    "            Nullam ut rhoncus justo, sed pretium lacus. Praesent vehicula purus nunc. Suspendisse ultricies faucibus\n" +
    "            sagittis. Vivamus luctus malesuada justo, eu aliquet sapien. Curabitur tincidunt rhoncus lorem, non volutpat\n" +
    "            sem ultricies at. In sit amet libero ut erat sagittis maximus at vitae nisl. Pellentesque habitant morbi\n" +
    "            tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus vel erat tortor.\n" +
    "\n" +
    "            Praesent id enim velit. Mauris elementum mauris eu elit placerat, et tincidunt eros maximus. Nulla porttitor\n" +
    "            elit non elementum pharetra. Aliquam ut venenatis justo, ut iaculis nibh. Donec sed nibh scelerisque, ornare\n" +
    "            elit eget, condimentum nunc. Vestibulum sed condimentum quam. Curabitur vel commodo tortor. Sed aliquam\n" +
    "            efficitur pretium. Nulla eu tortor dapibus libero pharetra congue.\n" +
    "\n" +
    "            Sed efficitur, enim molestie suscipit efficitur, lorem nisi vehicula odio, sit amet efficitur arcu ligula\n" +
    "            consectetur orci. Etiam placerat vulputate nisl, eu vulputate mauris malesuada ut. Vestibulum quis sem\n" +
    "            bibendum, consectetur diam ac, sagittis lectus. Fusce ac congue velit. In at quam et nisi tempus lacinia.\n" +
    "            Nullam turpis ipsum, convallis venenatis enim quis, lacinia pulvinar nulla. Praesent eu quam elementum,\n" +
    "            mattis orci nec, volutpat mi. Sed in congue orci, eget ultricies nisl. Pellentesque vel ex ipsum. In in\n" +
    "            tortor at arcu tristique gravida et eget massa. Maecenas quis luctus est. Sed nunc enim, porta blandit metus\n" +
    "            eu, consequat molestie ex. Duis et nunc varius, tempor risus ac, suscipit eros. Praesent malesuada quam sit\n" +
    "            amet condimentum imperdiet.\n" +
    "\n" +
    "            Proin at augue faucibus, volutpat lorem non, porta est. Vestibulum fringilla, odio a maximus lacinia, urna\n" +
    "            sem congue nisl, quis tempus dui erat quis lacus. Integer varius iaculis justo nec ornare. Phasellus non\n" +
    "            tortor eu risus tristique euismod. Nulla massa quam, tempor quis hendrerit sed, tristique at libero. Vivamus\n" +
    "            vitae mollis felis. Donec sit amet massa quis tortor finibus vulputate.";

export class Navbar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {activeItem: this.props.activeMenuItem, isPopUpActive: false};
    }

    closePopUp = () => {
        window.history.back();
        this.setState({activeItem: path});
        this.setState({isPopUpActive: false});
    };

    onClickHandler = (e, {name}) => {
        this.setState({activeItem: name});
        if (name === 'contacts') {
            let pathArray = window.location.pathname.split('/');
            path = pathArray[1];
            window.history.pushState("", "", '/contacts');
            this.setState({isPopUpActive: true});
        }
        else {
            this.setState({isPopUpActive: false});
        }
    };

    render() {
        const {activeItem} = this.state;
        return (
            < div className="mainMenu">
                <Grid celled>
                    <Grid.Row>
                        <Grid.Column width={3}>
                        </Grid.Column>
                        <Grid.Column width={10}>
                            <h2>Bumin Yazılım</h2>
                        </Grid.Column>
                        <Grid.Column width={3}>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={3}>
                            < Menu text vertical>
                                < Link
                                    to="/home">
                                    < Menu.Item
                                        name='home'
                                        active={activeItem === 'home'}
                                        color={'red'}
                                        onClick={this.onClickHandler}
                                    />
                                </Link>

                                <Link to="/about">
                                    <Menu.Item
                                        name='about'
                                        active={activeItem === 'about'}
                                        color={'red'}
                                        onClick={this.onClickHandler}
                                    />
                                </Link>

                                <Link to="/products">

                                    <Menu.Item
                                        name='products'
                                        active={activeItem === 'products'}
                                        color={'red'}
                                        onClick={this.onClickHandler}

                                    />
                                </Link>

                                <Menu.Item
                                    name='contacts'
                                    active={activeItem === 'contacts'}
                                    color={'red'}
                                    onClick={this.onClickHandler}
                                />

                            </Menu>
                        </Grid.Column>
                        <Grid.Column width={10}>

                            <Route exact path="/" component={Home}/>
                            <Route path="/home" component={Home}/>
                            <Route path="/about" component={About}/>
                            <Route path="/products" component={Products}/>

                            {this.state.isPopUpActive ?
                                <div className="circleDiv">
                                    <p><Icon link name='close' onClick={this.closePopUp}/></p>
                                    <h4>Contact Page</h4>
                                    <p id="mail-content">test@test.com</p>
                                    <p>Notes</p>
                                    <p>1)</p>
                                    <p>2)</p>

                                </div>
                                : null}

                        </Grid.Column>
                        <Grid.Column width={3}>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>


            </div>
        )
    }
}

const Home = () => (
    <div>
        <h2>Home</h2>
        <p>
            {content}
        </p>
    </div>
);
const About = () => (
    <div>
        <h2>About</h2>
        <p>
            {content}
        </p>
    </div>
);
const Products = () => (
    <div>
        <h2>Products</h2>
        <p>
            {content}
        </p>
    </div>
);
